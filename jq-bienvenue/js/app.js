

// Vous devez récuperer les valeurs des champs du formulaire, 
// en faire un joli objet et afficher le résultat dans la balise div#render.

var input=$("input")
var first = $("#first_name");
var last = $("#last_name");
var city = $("#city");
var pretty_object = new Object();

$(input).keypress(function (e) {

	if (e.which == 13) {
		
		pretty_object.FirstName = first.val();
		pretty_object.LastName = last.val();
		pretty_object.City = city.val() ;
		
		var render=pretty_object.FirstName + ' ' + pretty_object.LastName + ' de '+ pretty_object.City ;
		$("#username").replaceWith(render);		
		$("#render").css({ 'color': 'purple', 'font-size': '150%' });
	
	}
})


