var species = {
	cat    : "cat.jpg",
	bear   : "bear.jpg",
	fish   : "fish.jpg"
};

/* 
	tout votre code doit se trouver dans cette fonction,
	vous pouvez biensur créer d'autres fonctions si nécessaire
	*/
function main(){
	// 1. placez un listener sur le click des éléments <button>

	$("button").click(function(){
		// 2. dans le callback récupérez le data attribute animal
		var type = $(this).attr("data-animal"); 
		console.log(type);

		// 3. récupérerez dans l'objet species la valeur correspond à l'attribut récupéré
		var nameimg = species[type];
		console.log(nameimg);

		// 4. ajoutez élément img en assignant à son attibut src la valeur précédemment récupérée

		var url = '<img id="img" src="./img/' + nameimg + '"' + ' >'
				console.log(url);

		// 5. inserez cet élément dans l'élément ayant l'id holder
			if ($("#holder img").length == 0) {

			$('#holder').prepend(url);
		}
			else {
			$('#holder img').replaceWith(url);
		}
	})
	
// Conseils : avancez étape par étape, console.log et debugger autant que nécessaires !
		 
}

$(document).ready(function(){
	main();
});