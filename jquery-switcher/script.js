if (typeof $ === 'undefined') {
	alert('RTFM');
	alert('lis le README.md');
}

const check = (event) => {
	const body = document.querySelector("body")
	let sad = document.querySelector(".sad-kangoo")
	if(!event.isOpen){
		var holder = document.createElement('div');
		holder.classList.add('sad-kangoo');
		holder.style = "position: absolute";
		var kangoo = document.createElement('img');
		kangoo.setAttribute('src', './img/SaveTreeKangaroo.png');
		holder.appendChild(kangoo);
		body.prepend(holder);
	}else if(sad) {
		body.removeChild(sad);
	}
}

if(!window.devtools.isOpen) check(window.devtools);

window.addEventListener('devtoolschange', event => {
	check(event.detail);
});
